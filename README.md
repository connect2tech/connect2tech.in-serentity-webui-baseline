connect2tech.in-Cucumber-BDD
============================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.


# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


# Important Links
- [Bitbucket Repositories](https://bitbucket.org/connect2tech)
- [Serenity Documentation](http://thucydides.info/docs/serenity/#introduction)

### Examples
- MyFirstTest.java - Simple program using @Test and opens a URL.
- LocatorExampleTest.java - Simple program using @Test, autowiring, PageObject, WebElement
- FacadeLocatorExampleTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade
- DropDownTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to select drop down.
- CheckBoxTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to handle check box.
- RadioButtonTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to handle radio button.
- TableTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, different methods to access html table.
- AlertsTest.java - Simple program using @Test, autowiring, PageObject, WebElementFacade, handling different alerts.

### Examples: BDD
- ShoppingCartRunner.java - Simple program using @Given, @When, @Then, @Step and @Pending 