package com.gettingstarted.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class FacadeLocatorExampleTest {
	
	@Managed
	WebDriver driver;
	
	FacadeLocatorExamplePage page;
	
	@Title("This is my first serenity test method!!!!!")
	@Test
	public void navigateToGooglePage() throws InterruptedException {
		page.selenium_clickOnLink();
	}
}
