package com.gettingstarted.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;

public class LocatorExamplePage extends PageObject {
	public LocatorExamplePage(WebDriver driver) {
		super();
	}

	public void selenium_clickOnLink() {
		
		open();
		
		WebElement we = getDriver().findElement(By.xpath("/html/body/div[1]/div/div[2]/nav/div/div[2]/ul[2]/a"));
		we.click();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
