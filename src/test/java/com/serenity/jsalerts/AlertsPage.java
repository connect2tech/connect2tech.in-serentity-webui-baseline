package com.serenity.jsalerts;

import org.fluentlenium.core.Alert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


@DefaultUrl("/htmlElements")
public class AlertsPage extends PageObject {

	@FindBy(id ="simple")
	private WebElementFacade regularAlertButton;
	
	@FindBy(id ="confirm")
	private WebElementFacade confirmAlertButton;
	
	@FindBy(id ="prompt")
	private WebElementFacade promptAlertButton;
	
	public void regulerAlert() throws InterruptedException {
		
		open();
		regularAlertButton.click();
		
		System.out.println("getAlert().getText()------------------>"+getAlert().getText());
		Thread.sleep(3000);
		getAlert().accept();
		
		Thread.sleep(2000);
		
	}
	
	public void confirmAlert() throws InterruptedException {
		
		open();
		confirmAlertButton.click();
		
		System.out.println(getAlert().getText());
		
		Thread.sleep(3000);
		//Accept
	//	getAlert().accept();
		
		//Dismiss
		getAlert().dismiss();
		
		Thread.sleep(2000);
		
	}
	
	public void promptAlert() throws InterruptedException {
		
		open();
		promptAlertButton.click();
		
		System.out.println(getAlert().getText());
		
		Thread.sleep(3000);
		
		getAlert().sendKeys("HELLO WORLD, Am i entered??");
		
		
		//Accept
		getAlert().accept();
		
		//Dismiss
	//	getAlert().dismiss();
		
		Thread.sleep(4000);
		
	}
}
