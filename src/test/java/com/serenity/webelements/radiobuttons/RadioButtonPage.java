package com.serenity.webelements.radiobuttons;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.webelements.RadioButtonGroup;

@DefaultUrl("/users/add")
public class RadioButtonPage extends PageObject {

	public void _radioButtonEx() throws InterruptedException {

		// getDriver().get("http://html.cita.illinois.edu/nav/form/radio/index.php?example=6");

		open();
		
		List<WebElement> radioButtons = getDriver().findElements(By.name("number"));

		RadioButtonGroup group = new RadioButtonGroup(radioButtons);

		group.selectByValue("2");

		System.out.println("group.getSelectedValue().get()--------------->" + group.getSelectedValue().get());

		Thread.sleep(5000);
	}

	public void _radioButtonEx2() throws InterruptedException {

		// getDriver().get("http://html.cita.illinois.edu/nav/form/radio/index.php?example=6");

		inRadioButtonGroup("crust").selectByValue("thin");
		System.out.println(inRadioButtonGroup("crust").getSelectedValue().get());

		Thread.sleep(2000);
	}
}
