package com.synchronization.page;

import org.openqa.selenium.WebDriver;

import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

//https://groups.google.com/forum/#!searchin/thucydides-users/setImplicitTimeout|sort:date/thucydides-users/JwKd6mQk9qg/ABJN8oXyCAAJ

@DefaultUrl("/users/add")
public class ImplicitWaitExamplePage extends PageObject {

	public ImplicitWaitExamplePage(WebDriver driver) {
		super();
	}

	//@FindBy(css = "#start > button")
	@FindBy(id = "name1")
	private WebElementFacade name;

	//@FindBy(xpath = "(//h42)[2]")
	//private WebElementFacade heading;

	public void _implictWaitExample1() throws InterruptedException {

		try {

			open();

			TemporalUnit t = ChronoUnit.SECONDS;
			setImplicitTimeout(6, t);
			System.out.println(
					"getImplicitWaitTimeout().toMinutes()------------------->" + getImplicitWaitTimeout().toMillis());
			name.sendKeys("hello");

			//System.out.println(heading.getText().toUpperCase());
			Thread.sleep(5000);

		} finally {
			// resetImplicitTimeout();
		}

	}

	public void _implictWaitExample2() throws InterruptedException {

		try {

			open();
			System.out.println(getImplicitWaitTimeout().toString());
			// startButton.click();
			// Thread.sleep(5000);

		} finally {

		}

	}

}
