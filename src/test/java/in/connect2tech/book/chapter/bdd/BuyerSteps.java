package in.connect2tech.book.chapter.bdd;

import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Step;

public class BuyerSteps {

	@Pending
	@Step("User Open the URL")
	public void openUrl() {

	}

	@Pending
	@Step("User Enters the Search Item in Search Box")
	public void typeSearchItemInSearchBox() {

	}

}
