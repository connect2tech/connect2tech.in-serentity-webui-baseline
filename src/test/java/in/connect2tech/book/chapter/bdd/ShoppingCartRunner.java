package in.connect2tech.book.chapter.bdd;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		features = "src/test/resources/features/my_shopping_cart/shopping_cart.feature", 
		glue = { "in.connect2tech.book.chapter.bdd" })
public class ShoppingCartRunner {
}
