Feature: Shopping Cart Implementation 

	This is feature description. This is coming for feature file.

Scenario: Show shipping cost for an item in the shopping cart 
	Given I have searched for 'docking station' 
	And I have selected a matching item 
	When I add it to the cart 
	Then the shipping cost should be included in the total price 
